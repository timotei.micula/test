var s;
var a = 97;

function start() {
    s = setInterval(letters, 100);
}

function letters() {
    var div = document.querySelector("#letter");
    div.innerText = String.fromCharCode(a);
    a++;
    if (a == 122) a = 97;
    div.style.color = "rgb(" + Math.floor(Math.random() * 256) + "," + Math.floor(Math.random() * 256) + "," + Math.floor(Math.random() * 256) + ")";
}

function stop() {
    clearInterval(s);
}
